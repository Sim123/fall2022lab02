package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

/**
 *	  Greeting is a class used for testing packages out
 *  @author   Simona Dragomir
 *  @version  8/31/2022
*/


public class Greeter {
    public static void main(String[] args) {
    Scanner obj = new Scanner(System.in);
    Random ran = new Random();
    System.out.println("Enter Number");
    //this code stores the integer the user enters
    int num1 = obj.nextInt();
    int num2 = ran.nextInt(10+1);
    System.out.println("The Number is: " + num1);
    System.out.println("The Random Number is: " + num2);
    //this code uses the package
    Utilities test = new Utilities();
    System.out.println("The Doubled Number is: " + test.doubleMe(num1));
    }
}
