package secondpackage;

/**
 *	  Utilities is part of a package
 *  @author   Simona Dragomir
 *  @version  8/31/2022
*/

public class Utilities {
    private int theNum;
    //constrictor that sets theNum's value to 0
    public Utilities() {
        this.theNum = 0;
    }

    /**
     *  Doubles the input value
     *
     *  @param  x   The value to be doubled
     *  @return The value of x times two
     */

    // methode that recives doubles an integer and returns it
    public int doubleMe(int x) {
        this.theNum = x*2;
        return this.theNum;
    }
}
